# IT.MEDNSK

Добро пожаловать в репозиторий IT.MEDNSK

Здесь будут собраны типовые скрипты, имеющие применение в сообществе IT.MEDNSK

Инструкции находятся в разделе [Вики](https://git.neuronmaster.xyz/neuronmaster/IT.MEDNSK/wiki/Home)

Вы можете помочь проекту:

* Написать полезную статью
* Поделиться скриптами
* Выложить инструкцию

По вопросам предоставления доступа для редактирования обращаться: 

* [email: nasavushkin@mznso.ru](mailto:nasavushkin@mznso.ru)
* [tg: @neuronmaster](https://t.me/neuronmaster)
