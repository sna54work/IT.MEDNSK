Import-Csv C:\ps\ResetPass.csv -Delimiter ";" | Foreach {
$NewPass = ConvertTo-SecureString -AsPlainText $_.NewPassword -Force
Set-ADAccountPassword -Identity $_.sAMAccountName -NewPassword $NewPass -Reset -PassThru | Set-ADUser -ChangePasswordAtLogon $false
}
